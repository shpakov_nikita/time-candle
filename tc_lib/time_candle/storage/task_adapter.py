from time_candle.storage.adapter_classes import (
    Task,
    UserProjectRelation,
    Project,
    USE_SQL,
    _db_proxy,
)
from time_candle.storage.adapter_classes import Filter as PrimaryFilter
from time_candle.storage.adapter_classes import Adapter as PrimaryAdapter
from time_candle.model.instances.task import Task as TaskInstance
from time_candle.storage import logger
import time_candle.exceptions.db_exceptions as db_e
from peewee import (
    DoesNotExist,
    IntegrityError,
    OperationalError,
)


class TaskFilter(PrimaryFilter):
    OP_GREATER = 0
    OP_GREATER_OR_EQUALS = 1
    OP_EQUALS = 2
    OP_LESS_OR_EQUALS = 3
    OP_LESS = 4
    OP_NOT_EQUALS = 5

    def __init__(self):
        super().__init__()

    @staticmethod
    def _union_filter():
        return Task.tid.is_null(False)

    def tid(self, tid, op=PrimaryFilter.OP_AND):
        if self.result:
            self.ops.append(op)

        if isinstance(tid, (list,)):
            self.result.append(Task.tid << tid)
        else:
            self.result.append(Task.tid == tid)

        return self

    def creator(self, uid, op=PrimaryFilter.OP_AND):
        if self.result:
            self.ops.append(op)

        if isinstance(uid, (list,)):
            self.result.append(Task.creator << uid)
        else:
            self.result.append(Task.creator == uid)

        return self

    def receiver(self, uid, op=PrimaryFilter.OP_AND):
        if self.result:
            self.ops.append(op)

        if isinstance(uid, (list,)):
            self.result.append(Task.receiver << uid)
        else:
            self.result.append(Task.receiver == uid)

        return self

    def project(self, pid, op=PrimaryFilter.OP_AND):
        if self.result:
            self.ops.append(op)

        if isinstance(pid, (list,)):
            try:
                if pid[0] is None:
                    self.result.append(Task.project.is_null(True))
                else:
                    self.result.append(Task.project << pid)
            except IndexError:
                raise ValueError('Ooops. Unexpected list without args')

        elif pid is None:
            self.result.append(Task.project.is_null(True))
        else:
            self.result.append(Task.project == pid)

        return self

    def status(self,
               status,
               storage_op=OP_EQUALS,
               op=PrimaryFilter.OP_AND):
        # in this function we also have to compare on less or greater, that's
        # why another operator arg is needed
        if self.result:
            self.ops.append(op)

        if TaskFilter.OP_GREATER == storage_op:
            self.result.append(Task.status > status)

        elif TaskFilter.OP_GREATER_OR_EQUALS == storage_op:
            self.result.append(Task.status >= status)

        elif TaskFilter.OP_EQUALS == storage_op:
            self.result.append(Task.status == status)

        elif TaskFilter.OP_LESS_OR_EQUALS == storage_op:
            self.result.append(Task.status <= status)

        elif TaskFilter.OP_LESS == storage_op:
            self.result.append(Task.status < status)

        elif TaskFilter.OP_NOT_EQUALS == storage_op:
            self.result.append(Task.status != status)

        else:
            raise db_e.InvalidFilterOperator(db_e.FilterMessages.
                                             FILTER_DOES_NOT_EXISTS)

        return self

    def parent(self, tid, op=PrimaryFilter.OP_AND):
        if self.result:
            self.ops.append(op)

        if isinstance(tid, (list,)):
            self.result.append(Task.parent << tid)
        else:
            self.result.append(Task.parent == tid)

        return self

    def title_substring(self, substring, op=PrimaryFilter.OP_AND):
        if self.result:
            self.ops.append(op)

        self.result.append(Task.title.contains(substring))
        return self

    def title_regex(self, regex, op=PrimaryFilter.OP_AND):
        if self.result:
            self.ops.append(op)

        self.result.append(Task.title.contains(regex))
        return self

    def priority(self,
                 priority,
                 storage_op=OP_EQUALS,
                 op=PrimaryFilter.OP_AND):
        # in this function we also have to compare on less or greater, that's
        # why another operator arg is needed
        if self.result:
            self.ops.append(op)

        if TaskFilter.OP_GREATER == storage_op:
            self.result.append(Task.priority > priority)

        elif TaskFilter.OP_GREATER_OR_EQUALS == storage_op:
            self.result.append(Task.priority >= priority)

        elif TaskFilter.OP_EQUALS == storage_op:
            self.result.append(Task.priority == priority)

        elif TaskFilter.OP_LESS_OR_EQUALS == storage_op:
            self.result.append(Task.priority <= priority)

        elif TaskFilter.OP_LESS == storage_op:
            self.result.append(Task.priority < priority)

        elif TaskFilter.OP_NOT_EQUALS == storage_op:
            self.result.append(Task.priority != priority)

        else:
            raise db_e.InvalidFilterOperator(db_e.FilterMessages.
                                             FILTER_DOES_NOT_EXISTS)

        return self

    def deadline_time(self,
                      deadline_time,
                      storage_op=OP_EQUALS,
                      op=PrimaryFilter.OP_AND):
        # in this function we also have to compare on less or greater, that's
        # why another operator arg is needed
        if self.result:
            self.ops.append(op)

        if TaskFilter.OP_GREATER == storage_op:
            self.result.append(Task.deadline_time > deadline_time)

        elif TaskFilter.OP_GREATER_OR_EQUALS == storage_op:
            self.result.append(Task.deadline_time >= deadline_time)

        elif TaskFilter.OP_EQUALS == storage_op:
            self.result.append(Task.deadline_time == deadline_time)

        elif TaskFilter.OP_LESS_OR_EQUALS == storage_op:
            self.result.append(Task.deadline_time <= deadline_time)

        elif TaskFilter.OP_LESS == storage_op:
            self.result.append(Task.deadline_time < deadline_time)

        elif TaskFilter.OP_NOT_EQUALS == storage_op:
            self.result.append(Task.deadline_time != deadline_time)

        else:
            raise db_e.InvalidFilterOperator(db_e.FilterMessages.
                                             FILTER_DOES_NOT_EXISTS)

        return self

    def creation_time(self,
                      creation_time,
                      storage_op=OP_EQUALS,
                      op=PrimaryFilter.OP_AND):
        # in this function we also have to compare on less or greater, that's
        # why another operator arg is needed
        if self.result:
            self.ops.append(op)

        if TaskFilter.OP_GREATER == storage_op:
            self.result.append(Task.creation_time > creation_time)

        elif TaskFilter.OP_GREATER_OR_EQUALS == storage_op:
            self.result.append(Task.creation_time >= creation_time)

        elif TaskFilter.OP_EQUALS == storage_op:
            self.result.append(Task.creation_time == creation_time)

        elif TaskFilter.OP_LESS_OR_EQUALS == storage_op:
            self.result.append(Task.creation_time <= creation_time)

        elif TaskFilter.OP_LESS == storage_op:
            self.result.append(Task.creation_time < creation_time)

        elif TaskFilter.OP_NOT_EQUALS == storage_op:
            self.result.append(Task.creation_time != creation_time)

        else:
            raise db_e.InvalidFilterOperator(db_e.FilterMessages.
                                             FILTER_DOES_NOT_EXISTS)

        return self

    def comment_substring(self, substring, op=PrimaryFilter.OP_AND):
        if self.result:
            self.ops.append(op)

        self.result.append(Task.comment.contains(substring))
        return self

    def comment_regex(self, substring, op=PrimaryFilter.OP_AND):
        if self.result:
            self.ops.append(op)

        self.result.append(Task.comment.contains(substring))
        return self

    def period(self,
               period,
               storage_op=OP_EQUALS,
               op=PrimaryFilter.OP_AND):
        # in this function we also have to compare on less or greater, that's
        # why another operator arg is needed
        if self.result:
            self.ops.append(op)

        if TaskFilter.OP_GREATER == storage_op:
            self.result.append(Task.period > period)

        elif TaskFilter.OP_GREATER_OR_EQUALS == storage_op:
            self.result.append(Task.period >= period)

        elif TaskFilter.OP_EQUALS == storage_op:
            self.result.append(Task.period == period)

        elif TaskFilter.OP_LESS_OR_EQUALS == storage_op:
            self.result.append(Task.period <= period)

        elif TaskFilter.OP_LESS == storage_op:
            self.result.append(Task.period < period)

        elif TaskFilter.OP_NOT_EQUALS == storage_op:
            self.result.append(Task.period != period)

        else:
            raise db_e.InvalidFilterOperator(db_e.FilterMessages.
                                             FILTER_DOES_NOT_EXISTS)

        return self


class TaskAdapter(PrimaryAdapter):
    def __init__(self,
                 db_name=None,
                 uid=None,
                 psql_config=None,
                 connect_url=None):
        super().__init__(uid, db_name, psql_config, connect_url)

    def get_by_filter(self, filter_instance):
        """
        This function returns storage objects by the given TaskFilter.
        :param filter_instance: TaskFilter with defined filters
        :return: List of Task objects
        """
        # in this query we get all available tasks (our personal tasks and all
        # tasks our in projects)
        query = self._get_available_tasks(use_sql=False).select(). \
            where(filter_instance.to_query())
        # return converted query to the outer module
        return [TaskInstance.make_task(task) for task in query]

    def has_rights(self, tid, use_sql=USE_SQL):
        """
        This function returns bool value for current user. It checks if he can
        modify this task or not
        :param use_sql: Use sql raw query?
        :param tid: Task's id
        :return: Bool
        """
        try:
            if use_sql:
                cursor = _db_proxy.execute_sql('select * from task where '
                                               'task.tid = {tid};'.
                                               format(tid=tid))

                task = Task.from_tuple(cursor.fetchone())
            else:
                task = Task.select().where(Task.tid == tid).get()

        except (DoesNotExist, OperationalError):
            raise db_e.InvalidTidError(
                db_e.TaskMessages.TASK_DOES_NOT_EXISTS)

        if task.receiver == self.uid:
            return True

        elif task.project:
            if use_sql:
                cursor = _db_proxy.execute_sql('select * from project where '
                                               'project.pid = {pid};'.
                                               format(pid=task.project))

                project = Project.from_tuple(cursor.fetchone())
            else:
                project = Project.select().where(
                    Project.pid == task.project).get()

            return project.admin == self.uid

        else:
            return task.creator == self.uid

    def save(self, obj, use_sql=USE_SQL):
        """
        This function is used to store given task to the database. Note, that
        tasks can be with similar names and dates (but on that case I have a
        task below)
        :param obj: type with fields:
        - creator_uid
        - uid
        - pid
        - status
        - deadline_time
        - title
        - priority
        - parent with tid or None
        - comment
        - realization_time or None
        - creation_time
        :return: None
        """
        print("================SAVE====================")
        try:
            if use_sql:
                cursor = _db_proxy.execute_sql('select * from task where '
                                               'task.tid = {tid} and '
                                               '(task.creator = {uid} '
                                               'or task.receiver = {uid});'.
                                               format(tid=obj.tid,
                                                      uid=self.uid))

                task = Task.from_tuple(cursor.fetchone())
                print(task)

            else:
                task = Task.select().where((Task.tid == obj.tid) &
                                           ((Task.receiver == self.uid) |
                                            (Task.creator == self.uid))).get()

            TaskAdapter._update(task, obj, use_sql)

            logger.debug('task updated')
            return

        except (DoesNotExist, OperationalError) as e:
            print(str(e))
            logger.debug('adding task...')
        try:
            # check that if receiver is not us, that we have rights to do smt in
            # the project
            if obj.pid is not None:
                # But we hope that we are already checked this
                pass

            if use_sql:
                def cast_if_none(val):
                    return val if val is not None else 'null'

                current_tid = self.last_id(use_sql=True) + 1

                def make_tup(_obj):
                    return (
                        current_tid,
                        cast_if_none(_obj.creator_uid),
                        cast_if_none(_obj.uid),
                        cast_if_none(_obj.pid),
                        cast_if_none(_obj.status),
                        cast_if_none(_obj.parent),
                        cast_if_none(_obj.title),
                        cast_if_none(_obj.priority),
                        cast_if_none(_obj.deadline),
                        cast_if_none(_obj.realization_time),
                        cast_if_none(_obj.creation_time),
                        cast_if_none(_obj.comment),
                        cast_if_none(_obj.period),
                    )

                tup = str(make_tup(obj))
                tup = tup.replace("'null'", "null")
                query_str = 'insert into task values {tup};'.format(tup=tup)
                _db_proxy.execute_sql(query_str)
                cursor = _db_proxy.execute_sql('select * from task where '
                                               'task.tid = {tid};'.
                                               format(tid=current_tid))

                table_task = Task.from_tuple(cursor.fetchone())

            else:
                table_task = Task.create(creator=obj.creator_uid,
                                         receiver=obj.uid,
                                         project=obj.pid,
                                         status=obj.status,
                                         parent=obj.parent,
                                         title=obj.title,
                                         priority=obj.priority,
                                         deadline_time=obj.deadline,
                                         comment=obj.comment,
                                         realization_time=obj.realization_time,
                                         creation_time=obj.creation_time,
                                         period=obj.period)

        except (IntegrityError, OperationalError):
            # if you are guest
            raise db_e.InvalidLoginError(db_e.TaskMessages.DO_NOT_HAVE_RIGHTS)

        logger.debug('task\'s parent %s', str(table_task.parent))

        logger.debug('task saved to database')

    def get_task_by_id(self, tid, use_sql=USE_SQL):
        """
        This function finds task by id and current user in database and returns
        it, or raise error due to incorrect request
        :param tid: Task id to find
        :type tid: Int
        :return: Task
        """
        try:
            if use_sql:
                cursor = _db_proxy.execute_sql('select * from task where task.tid '
                                               '= {tid} and (task.creator = {uid} '
                                               'or task.receiver = {uid});'.
                                               format(tid=tid, uid=self.uid))

                task = Task.from_tuple(cursor.fetchone())

            else:
                task = Task.select().where((Task.tid == tid) &
                                           ((Task.creator == self.uid) |
                                            (Task.receiver == self.uid))).get()

            return TaskInstance.make_task(task)

        except (DoesNotExist, OperationalError):
            logger.info('There is no such tid %s in the database for your user',
                        tid)
            try:
                # if we can find such tid, then we don't have rights
                if use_sql:
                    tasks = tuple(
                        [task.tid for task in self._get_available_tasks()])
                    cursor = _db_proxy.execute_sql(
                        'select * from task where (task.tid == {tid}) and '
                        '({tid} in {tasks_tup});'.format(tid=tid,
                                                        tasks_tup=str(tasks)))
                    Task.from_tuple(cursor.fetchone())
                else:
                    self._get_available_tasks().select().\
                        where(Task.tid == tid).get()

                raise db_e.InvalidTidError(db_e.TaskMessages.DO_NOT_HAVE_RIGHTS)

            except (DoesNotExist, OperationalError):
                raise db_e.InvalidTidError(
                    db_e.TaskMessages.TASK_DOES_NOT_EXISTS)

    def _get_available_tasks(self, use_sql=USE_SQL):
        # get all tasks in project related to user and his personal tasks
        if use_sql:
            cursor = _db_proxy.execute_sql('select * from userprojectrelation '
                                           'where userprojectrelation.user '
                                           '= {uid};'.format(uid=self.uid))
            projects = tuple([UserProjectRelation.from_tuple(tup).project.pid
                              for tup in cursor.fetchall()])

            query_str = 'select * from task where (task.project_id in {tup}) or ' \
                        '(task.creator = {uid});'.\
                format(tup=str(projects),uid=self.uid)
            print(query_str, 'hahaha')
            query = _db_proxy.execute_sql(query_str)
            query = [Task.from_tuple(tup) for tup in query.fetchall()]

        else:

            query_projects = UserProjectRelation. \
                select().where(UserProjectRelation.user == self.uid)
            projects = [rel.project.pid for rel in query_projects]
            query = Task.select().where((Task.project << projects) |
                                        (Task.creator == self.uid))
        return query

    @staticmethod
    def _update(task, obj, use_sql=USE_SQL):
        if use_sql:
            def cast_to_format(val):
                return "'" + val + "'" if isinstance(val, str) else \
                    val if val is not None else 'null'

            _full_sql_query_str = 'update task set ' \
                                  'creator = {creator_uid}, ' \
                                  'receiver = {uid}, ' \
                                  'project_id = {pid}, ' \
                                  'status = {status}, ' \
                                  'parent_id = {parent}, ' \
                                  'title = {title}, ' \
                                  'priority = {priority}, ' \
                                  'deadline_time = {deadline}, ' \
                                  'comment = {comment}, ' \
                                  'creation_time = {creation_time}, ' \
                                  'realization_time = {realization_time} ' \
                                  ' where tid = {tid};'.format(
                                      tid=task.tid,
                                      creator_uid=cast_to_format(obj.creator_uid),
                                      uid=cast_to_format(obj.uid),
                                      pid=cast_to_format(obj.pid),
                                      status=cast_to_format(obj.status),
                                      parent=cast_to_format(obj.parent),
                                      title=cast_to_format(obj.title),
                                      priority=cast_to_format(obj.priority),
                                      deadline=cast_to_format(obj.deadline),
                                      comment=cast_to_format(obj.comment),
                                      realization_time=cast_to_format(obj.realization_time),
                                      creation_time=cast_to_format(obj.creation_time),
                                      period=cast_to_format(obj.period))

            print(_full_sql_query_str)
            _db_proxy.execute_sql(_full_sql_query_str)

        else:
            task.creator = obj.creator_uid
            task.receiver = obj.uid
            task.project = obj.pid
            task.status = obj.status
            task.parent = obj.parent
            task.title = obj.title
            task.priority = obj.priority
            task.deadline_time = obj.deadline
            task.comment = obj.comment
            task.creation_time = obj.creation_time
            task.realization_time = obj.realization_time

            task.save()

    @staticmethod
    def last_id(use_sql=USE_SQL):
        """
        This function gets last id to add task on it's place
        TODO:
        This function finds the first unused id in the table to add new task row
        on that place
        :return: Int
        """
        if use_sql:
            cursor = _db_proxy.execute_sql('select * from task order by '
                                           'task.tid desc;')

            task_tup = cursor.fetchone()
            print('desc tasks: ', [task_tup].extend([Task.from_tuple(tup) for tup in cursor.fetchall()]))

        else:
            query = Task.select().order_by(Task.tid.desc())

        logger.debug('getting last id from query...')

        try:
            # task query
            if use_sql:
                return Task.from_tuple(task_tup).tid
            else:
                return query.get().tid

        except (DoesNotExist, OperationalError):
            return 0

    def remove_task_by_id(self, tid, use_sql=USE_SQL):
        """
        This function removes selected task and it's all childs recursively or
        raises an exception if task does not exists
        :param tid: Tasks id
        :return: None
        """
        try:
            if use_sql:
                query = filter(lambda t: t.parent.tid == tid if t.parent is not None else False,
                               self._get_available_tasks(use_sql=True))

                for task in query:
                    self.remove_task_by_id(task.tid, use_sql=True)

                logger.info('removing task by tid %s', tid)
                query = list(filter(lambda t: (t.tid == tid) and
                                              ((t.receiver == self.uid) or
                                               (t.creator == self.uid)),
                                    self._get_available_tasks(use_sql=True)))
                task = query[0]
                _db_proxy.execute_sql('delete from task where task.tid = {tid};'.
                                      format(tid=task.tid))
            else:
                query = self._get_available_tasks(use_sql=False).select().\
                    where(Task.parent == tid)
                for task in query:
                    self.remove_task_by_id(task.tid)

                logger.info('removing task by tid %s', tid)
                task = self._get_available_tasks(use_sql=False).\
                    select().where((Task.tid == tid) &
                                   ((Task.receiver == self.uid) |
                                    (Task.creator == self.uid))).get()

                task.delete_instance()

        except (DoesNotExist, OperationalError, IndexError):
            logger.info(
                'There is no such tid %s in the database for your user', tid)
            raise db_e.InvalidTidError(db_e.TaskMessages.TASK_DOES_NOT_EXISTS)
