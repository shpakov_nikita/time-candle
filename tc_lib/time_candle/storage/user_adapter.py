from time_candle.storage.adapter_classes import User
from time_candle.storage.adapter_classes import Message
from time_candle.storage.adapter_classes import Filter as PrimaryFilter
from time_candle.storage.adapter_classes import Adapter as PrimaryAdapter
from time_candle.storage.adapter_classes import (
    USE_SQL,
    _db_proxy,
)
from time_candle.model.instances.user import User as UserInstance
from time_candle.model.instances.message import Message as MessageInstance
from time_candle.storage import logger
import time_candle.exceptions.db_exceptions as db_e
from peewee import DoesNotExist


class UserFilter(PrimaryFilter):

    def __init__(self):
        super().__init__()

    @staticmethod
    def _union_filter():
        return User.uid.is_null(False)

    def uid(self, uid, op=PrimaryFilter.OP_AND):
        if self.result:
            self.ops.append(op)

        self.result.append(User.uid == uid)
        return self

    def login_substring(self, substring, op=PrimaryFilter.OP_AND):
        if self.result:
            self.ops.append(op)

        self.result.append(User.login.contains(substring))
        return self

    def login_regex(self, regex, op=PrimaryFilter.OP_AND):
        if self.result:
            self.ops.append(op)

        self.result.append(User.login.contains(regex))
        return self

    def nickname_substring(self, substring, op=PrimaryFilter.OP_AND):
        if self.result:
            self.ops.append(op)

        self.result.append(User.nickname.contains(substring))
        return self

    def nickname_regex(self, regex, op=PrimaryFilter.OP_AND):
        if self.result:
            self.ops.append(op)

        self.result.append(User.nickname.contains(regex))
        return self


class UserAdapter(PrimaryAdapter):
    def __init__(self,
                 db_name=None,
                 uid=None,
                 psql_config=None,
                 connect_url=None):
        super().__init__(uid, db_name, psql_config, connect_url)

    @staticmethod
    def get_by_filter(filter_instance):
        """
        This function returns storage objects by the given UserFilter.
        :param filter_instance: UserFilter with defined filters
        :return: List of UserInstance
        """
        query = User.select().where(filter_instance.to_query())

        return [UserInstance.make_user(user) for user in query]

    @staticmethod
    def save(obj, use_sql=USE_SQL):
        """
        This function if checking is current user exists, and if so we are
        raising an exception. Or we are adding it to the database.
        This function is used to store given task to the database. Note, that
        tasks can be with similar names and dates (but on that case I have a
        task below)
        :param obj: type with fields:
         - login
         - nickname,
         - about
        :return: None
        """

        if use_sql:
            cursor = _db_proxy.execute_sql('select * from "user" where uid=cast({uid} as text)'.format(uid=obj.uid))
            user_tup = cursor.fetchone()
            if user_tup is not None:
                UserAdapter._update(User.from_tuple(user_tup), obj, use_sql=use_sql)
                logger.debug('user updated')
                return
            
            logger.debug('adding user...')
            # we create about like "Hello, it's me, username"
            # we trying to escape ' symbol cause it will break query
            # let's replace it with '' which will be treated by postgres as one '
            # fix this kostil when we pass the lab
            obj.about = obj.about.replace("it's", "it''s")
            values_str = str((obj.uid, obj.login, obj.nickname, obj.about))
            # in obj.about ' symbol can be present, 
            # so in values_str obj.about will be wrapped in " 
            # it breaks postgres query and we try to replace it with '
            values_str = values_str.replace('"', "'")
            _db_proxy.execute_sql('insert into "user"(uid, login, nickname, about) values {values}'.format(
                values = values_str
            ))
        else:
            try:
                user = User.select().where(User.uid == obj.uid).get()

                UserAdapter._update(user, obj)

                logger.debug('user updated')
                return

            except DoesNotExist:
                logger.debug('adding user...')

            User.create(uid=obj.uid,
                        login=obj.login,
                        about=obj.about,
                        nickname=obj.nickname)

    @staticmethod
    def _update(user, obj, use_sql=USE_SQL):
        if use_sql:
            user.nickname = obj.nickname
            user.about = obj.about
            # we trying to escape ' symbol cause it will break query
            # let's replace it with '' which will be treated by postgres as one '
            user.about = user.about.replace("'", "''")

            _db_proxy.execute_sql('update "user" set nickname = \'{nickname}\', about = \'{about}\' '
                                  'where uid=cast({uid} as text)'.format(nickname=user.nickname, about=user.about, 
                                  uid=user.uid))
        else:
            user.nickname = obj.nickname
            user.about = obj.about
            user.save()

    @staticmethod
    def get_id_by_login(login, use_sql=USE_SQL):
        """
        This function checks if user by passed login exists in the database and
        returns user's id, or raises an error
        :param login: User's login
        :return: Int
        """
        if use_sql:
            cursor = _db_proxy.execute_sql('select * from "user" where login=\'{login}\''.format(login=login))
            user_tup = cursor.fetchone()
            if user_tup is None:
                raise db_e.InvalidLoginError(
                    db_e.LoginMessages.USER_DOES_NOT_EXISTS)
            return User.from_tuple(user_tup).uid
        else:
            try:
                return User.select().where(User.login == login).get().uid

            except DoesNotExist:
                raise db_e.InvalidLoginError(
                    db_e.LoginMessages.USER_DOES_NOT_EXISTS)

    @staticmethod
    def get_user_by_id(uid, use_sql=USE_SQL):
        """
        This function returns login of user by id
        :param uid: User's id
        :return: String
        """
        if use_sql:
            cursor = _db_proxy.execute_sql('select * from "user" where uid=cast({uid} as text)'.format(uid=uid))
            user_tup = cursor.fetchone()
            if user_tup is None:
                raise db_e.InvalidUidError(
                    db_e.LoginMessages.USER_DOES_NOT_EXISTS)
            obj = User.from_tuple(user_tup)
        else:
            query = User.select().where(User.uid == uid)
            try:
                # user instance
                obj = query.get()

            except DoesNotExist:
                raise db_e.InvalidUidError(
                    db_e.LoginMessages.USER_DOES_NOT_EXISTS)

        return UserInstance.make_user(obj)

    ############################################################################
    # MESSAGES (It is better not to create another adapter for this feature)   #
    ############################################################################

    def get_messages(self, use_sql=USE_SQL):
        """
        This function returns all messages for current user
        :return: List of messages
        """
        if use_sql:
            cursor = _db_proxy.execute_sql('select * from message where uid=cast({uid} as text)'.format(uid=self.uid))
            query = [Message.from_tuple(tup) for tup in cursor.fetchall()]
        else:
            query = Message.select().where(Message.uid == self.uid)
        return [MessageInstance.make_message(message) for message in query]

    def remove_message(self, mid, use_sql=USE_SQL):
        if use_sql:
            cursor = _db_proxy.execute_sql('select * from message where mid={mid} and uid=cast({uid} as text)'
                                            .format(mid=mid, uid=self.uid))
            if cursor.fetchone() is None:
                raise db_e.InvalidMidError(db_e.MidMessages.DO_NOT_HAVE_RIGHTS)
            
            _db_proxy.execute_sql('delete from message where mid={mid} and uid=cast({uid} as text)'
                                            .format(mid=mid, uid=self.uid))
            logger.debug('Message has been removed')
        else:
            try:
                message = Message.select().where(
                    (Message.mid == mid) & (Message.uid == self.uid)).get()

                message.delete_instance()
                logger.debug('Message has been removed')
            except DoesNotExist:
                raise db_e.InvalidMidError(db_e.MidMessages.DO_NOT_HAVE_RIGHTS)

    @staticmethod
    def send_message(uid, message_str, use_sql=USE_SQL):
        if use_sql:
            _db_proxy.execute_sql('insert into message(content, uid) values {values}'.format(
                values = (message_str, uid)
            ))
        else:
            Message.create(uid=uid,
                        content=message_str)
        logger.debug('Message has been sent')
