from peewee import DoesNotExist, IntegrityError
from time_candle.storage.adapter_classes import (
    User,
    Project,
    UserProjectRelation,
    Task,
    USE_SQL,
    _db_proxy,
)
from time_candle.storage.adapter_classes import Filter as PrimaryFilter
from time_candle.storage.adapter_classes import Adapter as PrimaryAdapter
from time_candle.model.instances.project import Project as ProjectInstance
from time_candle.model.instances.user import User as UserInstance
import time_candle.exceptions.db_exceptions as db_e
from time_candle.storage import logger


class ProjectFilter(PrimaryFilter):

    def __init__(self):
        super().__init__()

    @staticmethod
    def _union_filter():
        return Project.pid.is_null(False)

    def pid(self, pid, op=PrimaryFilter.OP_AND):
        if self.result:
            self.ops.append(op)

        self.result.append(Project.pid == pid)
        return self

    def admin(self, uid, op=PrimaryFilter.OP_AND):
        if self.result:
            self.ops.append(op)

        self.result.append(Project.admin == uid)
        return self

    def title_substring(self, substring, op=PrimaryFilter.OP_AND):
        if self.result:
            self.ops.append(op)

        self.result.append(Project.title.contains(substring))
        return self

    def title_regex(self, regex, op=PrimaryFilter.OP_AND):
        if self.result:
            self.ops.append(op)

        self.result.append(Project.title.regexp(regex))
        return self

    def description_substring(self, substring, op=PrimaryFilter.OP_AND):
        if self.result:
            self.ops.append(op)

        self.result.append(Project.description.contains(substring))
        return self

    def description_regex(self, regex, op=PrimaryFilter.OP_AND):
        if self.result:
            self.ops.append(op)

        self.result.append(Project.description.regexp(regex))
        return self


class ProjectAdapter(PrimaryAdapter):
    def __init__(self,
                 db_name=None,
                 uid=None,
                 psql_config=None,
                 connect_url=None):
        super().__init__(uid, db_name, psql_config, connect_url)

    def get_by_filter(self, filter_instance):
        """
        This function returns storage objects by the given ProjectFilter.
        :param filter_instance: ProjectFilter with defined filters
        :return: List of Project objects
        """
        # in this query we get all available projects
        query = self._get_available_projects(use_sql=False).select().\
            where(filter_instance.to_query())

        # return converted query to the outer module
        return [ProjectInstance.make_project(project) for project in query]

    def save(self, obj, use_sql=USE_SQL):
        """
        This function is used to store given project in database
        :param obj: type with fields:
        - pid
        - admin_uid
        - description
        - title
        :return: None
        """
        try:
            if use_sql:
                if not self.has_rights(obj.pid, use_sql=use_sql):
                    raise db_e.InvalidLoginError(
                        db_e.ProjectMessages.DO_NOT_HAVE_RIGHTS)

                cursor = _db_proxy.execute_sql('select * from project '
                                           'where project.pid '
                                           '= {pid}'.format(pid=obj.pid))
                project_tuple = cursor.fetchone()
                if project_tuple is not None:
                    self._update(Project.from_tuple(project_tuple), obj, use_sql=use_sql)
            else:
                if not self.has_rights(obj.pid):
                    raise db_e.InvalidLoginError(
                        db_e.ProjectMessages.DO_NOT_HAVE_RIGHTS)

                project = Project.select().where(Project.pid == obj.pid).get()
                self._update(project, obj)
            return
        except db_e.InvalidPidError:
            logger.debug('adding project...')

        if use_sql:
            if obj.admin_uid is None:
                raise db_e.InvalidLoginError(
                    db_e.ProjectMessages.DO_NOT_HAVE_RIGHTS)

            _db_proxy.execute_sql('insert into project(title, admin, description) values {values}'.format(
                    values = str((obj.title, obj.admin_uid, obj.description))
                ))
            
            new_project_id = self.last_id(use_sql)

            _db_proxy.execute_sql('insert into userprojectrelation("user", project_id) values {values}'.format(
                values = str((self.uid, new_project_id))
            ))
        else:
            try:
                project = Project(admin=obj.admin_uid,
                                description=obj.description,
                                title=obj.title)

                relation = UserProjectRelation(user=self.uid,
                                            project=project)

                # only if everything is ok we try save project to our database
                project.save()
                relation.save()

            except IntegrityError:
                # if you are guest
                raise db_e.InvalidLoginError(
                    db_e.ProjectMessages.DO_NOT_HAVE_RIGHTS)

        logger.debug('project saved to database')

    def get_project_by_id(self, pid, use_sql=USE_SQL):
        """
        This function finds project by id and current user in database and
        returns it or raise error due to incorrect request
        :param pid: Project id to find
        :type pid: Int
        :return: Project
        """
        if use_sql:
            cursor = _db_proxy.execute_sql('select * from userprojectrelation '
                                           'where userprojectrelation.project_id={pid} and userprojectrelation.user={uid}'
                                            .format(pid=pid, uid=self.uid))
            if len(cursor.fetchall()) == 0:
                logger.info('There is no such pid %s in the database for your user',
                            pid)
                raise db_e.InvalidPidError(
                    db_e.ProjectMessages.PROJECT_DOES_NOT_EXISTS)
            
            cursor = _db_proxy.execute_sql('select * from project '
                                           'where pid={pid}'.format(pid=pid))
            return ProjectInstance.make_project(Project.from_tuple(cursor.fetchone()))
        else:
            try:
                # we are checking if there is a connection our user and selected
                # project
                UserProjectRelation.select(). \
                    where((UserProjectRelation.user == self.uid) &
                        (UserProjectRelation.project == pid)).get()

                # if so, ge get this project by pid
                project = Project.select().where((Project.pid == pid))

                return ProjectInstance.make_project(project.get())

            except DoesNotExist:
                logger.info('There is no such pid %s in the database for your user',
                            pid)
                raise db_e.InvalidPidError(
                    db_e.ProjectMessages.PROJECT_DOES_NOT_EXISTS)

    def has_rights(self, pid, use_sql=USE_SQL):
        """
        This function checks if logged user has rights to do something inside
        the project (except deleting and modifying his own tasks)
        :param pid: Project's id
        :return: Bool
        """
        if use_sql:
            cursor = _db_proxy.execute_sql('select * from project '
                                           'where pid={pid}'.format(pid=pid))
            if len(cursor.fetchall()) == 0:
                raise db_e.InvalidPidError(
                    db_e.ProjectMessages.PROJECT_DOES_NOT_EXISTS)
        else:
            try:
                Project.select().where(Project.pid == pid).get()

            except DoesNotExist:
                raise db_e.InvalidPidError(
                    db_e.ProjectMessages.PROJECT_DOES_NOT_EXISTS)

        if use_sql:
            cursor = _db_proxy.execute_sql('select * from project '
                                           'where pid={pid} and admin={admin}'.format(pid=pid, admin=self.uid))
            return len(cursor.fetchall()) != 0
        else:
            try:
                Project.select().where((Project.pid == pid) &
                                    (Project.admin == self.uid)).get()

                return True

            except DoesNotExist:
                return False

    def add_user_to_project_by_id(self, uid, pid, use_sql=USE_SQL):
        """
        This function adds user to the Project relationship table to make user a
        member of the project, if both exists.
        :param uid: User's id
        :param pid: Project's id
        :return: None
        """
        if use_sql:
            cursor = _db_proxy.execute_sql('select * from project '
                                           'where pid={pid} and admin={admin}'.format(pid=pid, admin=self.uid))
            if len(cursor.fetchall()) == 0:
                raise db_e.InvalidPidError(
                    db_e.ProjectMessages.DO_NOT_HAVE_RIGHTS)
            logger.debug('such project exists')
        else:
            try:
                # we get task where current user is admin and where project id is
                # matching
                project = Project.select(). \
                    where((self.uid == Project.admin) &
                        (Project.pid == pid)).get()
                logger.debug('such project exists')

            except DoesNotExist:
                raise db_e.InvalidPidError(
                    db_e.ProjectMessages.DO_NOT_HAVE_RIGHTS)

        # NOTE: due to we don't have access to the users db, we BELIEVE, that
        # user exists in the database (you should check that on the auth level)

        if use_sql:
            cursor = _db_proxy.execute_sql('select * from userprojectrelation '
                                           'where userprojectrelation.project_id={pid} and userprojectrelation.user={uid}'
                                            .format(pid=pid, uid=uid))
            if len(cursor.fetchall()) != 0:
                logger.debug('such relation exists')
                raise db_e.InvalidLoginError(
                    db_e.ProjectMessages.USER_ALREADY_EXISTS)
            
            _db_proxy.execute_sql('insert into userprojectrelation("user", project_id) values {values}'.format(
                values = str((uid, pid))
            ))
        else:
            try:
                # and now we are checking if selected user already in the project.
                # If the exception DoesNotExist was not raised, that means that user
                # already in project and it's bad
                UserProjectRelation.select(). \
                    where((UserProjectRelation.project == pid) &
                        (UserProjectRelation.user == uid)).get()

                logger.debug('such relation exists')
                raise db_e.InvalidLoginError(
                    db_e.ProjectMessages.USER_ALREADY_EXISTS)
            except DoesNotExist:
                logger.debug('such user is not in project')

                UserProjectRelation.create(user=uid, project=project)

        logger.info('user_project relation created')

    def remove_from_project_by_id(self, uid, pid, use_sql=USE_SQL):
        """
        This function removes user from the project by it's id
        :param pid: Project's id
        :param uid: User's id
        :return: None
        """
        if use_sql:
            cursor = _db_proxy.execute_sql('select * from project '
                                           'where pid={pid} and admin={admin}'.format(pid=pid, admin=self.uid))
            if len(cursor.fetchall()) == 0:
                if uid != self.uid:
                    raise db_e.InvalidPidError(
                        db_e.ProjectMessages.DO_NOT_HAVE_RIGHTS)
            
            logger.debug('such project exists')

            if uid == self.uid:
                raise db_e.InvalidPidError(
                    db_e.ProjectMessages.DO_NOT_HAVE_RIGHTS)
        else:
            try:
                # we get task where current user is admin and where project id is
                # matching
                Project.select().where(
                    (self.uid == Project.admin) & (Project.pid == pid)).get()
                logger.debug('such project exists')
                # if an admin tries to delete himself we deny it
                if uid == self.uid:
                    raise db_e.InvalidPidError(
                        db_e.ProjectMessages.DO_NOT_HAVE_RIGHTS)

            except DoesNotExist:
                # not admin can try to delete himself
                if uid != self.uid:
                    raise db_e.InvalidPidError(
                        db_e.ProjectMessages.DO_NOT_HAVE_RIGHTS)

        if use_sql:
            cursor = _db_proxy.execute_sql('select * from userprojectrelation '
                                           'where userprojectrelation.project_id={pid} and userprojectrelation.user={uid}'.format(pid=pid,uid=uid))
            if len(cursor.fetchall()) == 0:
                raise db_e.InvalidUidError(db_e.LoginMessages.NO_USER_TO_DELETE)

            _db_proxy.execute_sql('delete from userprojectrelation '
                                  'where userprojectrelation.project_id={pid} and userprojectrelation.user={uid}'.format(pid=pid,uid=uid))
        else:
            # now we try to find and delete user
            rows = UserProjectRelation.delete().where(
                (UserProjectRelation.project == pid) &
                (UserProjectRelation.user == uid)).\
                execute()

            if rows == 0:
                raise db_e.InvalidUidError(db_e.LoginMessages.NO_USER_TO_DELETE)

    @staticmethod
    def is_user_in_project(uid, pid, use_sql=USE_SQL):
        """
        This function checks if passed user exists in the selected project
        :param uid: User's id
        :param pid: Project's id
        :return: Bool
        """
        logger.debug('the uid is %s', uid)

        if use_sql:
            cursor = _db_proxy.execute_sql('select * from userprojectrelation '
                                           'where userprojectrelation.project_id={pid} and userprojectrelation.user={uid}'.format(pid=pid,uid=uid))
            if len(cursor.fetchall()) == 0:
                raise db_e.InvalidPidError(db_e.LoginMessages.USER_DOES_NOT_EXISTS)

            logger.debug('user exists in project')
            return True
        else:
            try:
                UserProjectRelation.select(). \
                    where((UserProjectRelation.project == pid) &
                        (UserProjectRelation.user == uid)).get()

                logger.debug('user exists in project')
                return True

            except DoesNotExist:
                raise db_e.InvalidPidError(db_e.LoginMessages.USER_DOES_NOT_EXISTS)

    def get_users_by_project(self, pid, use_sql=USE_SQL):
        self.is_user_in_project(self.uid, pid, use_sql=use_sql)

        if use_sql:
            cursor = _db_proxy.execute_sql('select * from userprojectrelation '
                                           'where userprojectrelation.project_id={pid}'.format(pid=pid))
            user_project_rel_tup = cursor.fetchall()
            users = []
            for t in user_project_rel_tup:
                cursor = _db_proxy.execute_sql('select * from "user" where uid={uid}::text'.format(uid=t[1]))
                users.append(User.from_tuple(cursor.fetchone()))
        else:
            query = UserProjectRelation.select().\
                where(UserProjectRelation.project == pid)

            query = [q.user for q in query]
            users = User.select().where(User.uid << query)

        return [UserInstance.make_user(user) for user in users]


    def remove_project_by_id(self, pid, use_sql=USE_SQL):
        """
        This function removes project from id snd clears all relations between
        users and passed project. Or it raises an exception if you don't have
        rights or pid do not exists
        :param pid: Project's id
        :return: None
        """
        if use_sql:
            if not self.has_rights(pid, use_sql=use_sql):
                raise db_e.InvalidLoginError(
                    db_e.ProjectMessages.DO_NOT_HAVE_RIGHTS)

            # project existance was already checked in had_rights method above
            _db_proxy.execute_sql('delete from userprojectrelation '
                                  'where userprojectrelation.project_id={pid}'
                                    .format(pid=pid))
            _db_proxy.execute_sql('delete from task '
                                  'where task.project_id={pid}'
                                    .format(pid=pid))
            _db_proxy.execute_sql('delete from project where project.pid={pid}'
                                    .format(pid=pid))
            
            logger.debug('project fully removed')
        else:
            if not self.has_rights(pid):
                raise db_e.InvalidLoginError(
                    db_e.ProjectMessages.DO_NOT_HAVE_RIGHTS)

            try:
                UserProjectRelation.delete(). \
                    where(UserProjectRelation.project == pid).execute()

                Task.delete().where(Task.project == pid).execute()

                Project.delete().where(Project.pid == pid).execute()

                logger.debug('project fully removed')

            except DoesNotExist:
                db_e.InvalidPidError(db_e.ProjectMessages.PROJECT_DOES_NOT_EXISTS)

    @staticmethod
    def _update(project, obj, use_sql=USE_SQL):
        if use_sql:
            project.admin = obj.admin_uid
            project.description = obj.description
            project.title = obj.title

            _db_proxy.execute_sql("update project set title = '{title}', admin = {admin}, description = '{description}' "
                                  "where pid = {pid}".format(title=project.title, admin=project.admin, 
                                  description=project.description, pid=project.pid))
        else:
            project.admin = obj.admin_uid
            project.description = obj.description
            project.title = obj.title

            project.save()

    @staticmethod
    def last_id(use_sql=USE_SQL):
        """
        This function gets last id to add project on it's place
        TODO:
        This function finds the first unused id in the table to add new project
        row on that place
        :return: Int
        """
        if use_sql:
            cursor = _db_proxy.execute_sql('select * from project order by project.pid desc')
            project_tup = cursor.fetchone()
            if project_tup is None:
                return 0
            return Project.from_tuple(project_tup).pid
        else:
            query = Project.select().order_by(Project.pid.desc())
            logger.debug('getting last id from query...')

            try:
                # project query
                return query.get().pid

            except DoesNotExist:
                return 0

    def _get_available_projects(self, use_sql=USE_SQL):

        if use_sql:
            cursor = _db_proxy.execute_sql('select * from userprojectrelation '
                                           'where userprojectrelation.user '
                                           '= {uid}'.format(uid=self.uid))
            query_projects = [UserProjectRelation.from_tuple(tup).project for tup in cursor.fetchall()]
        else:
            # get all tasks in project related to user
            query_projects = Project.select(). \
                join(UserProjectRelation).\
                where(UserProjectRelation.user == self.uid)

        return query_projects
